/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
class PhotoAndShootCamera extends DigitalCamera{
    public PhotoAndShootCamera(String make,String model,double  megapix,int externalMemory){
       this.make=make;
        this.model=model;
        this.megapix=megapix;
        this.externalMemory=externalMemory;
    }
    
    @Override
    String describeCamera() {
        return "Make:" + make + " Model:"+model+" Megapix:"+megapix+" External Memory Size:"+externalMemory+"GB";
    }
    
}
