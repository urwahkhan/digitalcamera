/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
class PhoneCamera extends DigitalCamera{
public PhoneCamera(String make,String model,double  megapix,int internalMemory){
        this.make=make;
        this.model=model;
        this.megapix=megapix;
        this.internalMemory=internalMemory;
    }
    @Override
    String describeCamera() {
       return "Make:" + make + " Model:"+model+" Megapix:"+megapix+" Internal Memory Size:"+internalMemory+"GB";
    }
    
    
}
